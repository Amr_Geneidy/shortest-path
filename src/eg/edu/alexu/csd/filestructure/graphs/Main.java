package eg.edu.alexu.csd.filestructure.graphs;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        IGraph solver = new Graph();
        File inputFile = new File("res/graph_5_5.txt");
        int[] distances = new int[5];
        solver.readGraph(inputFile);
        solver.runDijkstra(0, distances);
        for (int i = 0; i < 5; i++) {
            System.out.println(distances[i]);
        }
    }
}
