package eg.edu.alexu.csd.filestructure.graphs;

import javax.management.RuntimeErrorException;
import java.io.*;
import java.util.*;

public class Graph implements IGraph {
    private List<List<Edge>> G;
    private List<List<Integer>> unweightedG;
    private int size, order;
    private List<Integer> verticesVal;
    private Vertex[] vertices;
    private List<Integer> processOrder;

    @Override
    public void readGraph(File file) {
        // TODO Auto-generated method stub
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            if ((st = br.readLine()) != null) {
                st = st.trim();
                String[] VE = st.split(" ");
                G = new ArrayList<>(Integer.parseInt(VE[0]));
                unweightedG = new ArrayList<>(Integer.parseInt(VE[0]));
                size = Integer.parseInt(VE[1]);
                order = Integer.parseInt(VE[0]);
                verticesVal = new ArrayList<>(order);
                vertices = new Vertex[order];
                for (int i = 0; i < order; i++) {
                    verticesVal.add(i);
                    vertices[i] = new Vertex(i, Integer.MAX_VALUE / 2, -1);
                }
                for (int i = 0; i < Integer.parseInt(VE[0]); i++) {
                    G.add(new ArrayList<>());
                    unweightedG.add(new ArrayList<>());
                }
            } else {
                br.close();
                throw new RuntimeErrorException(null);
            }
            int count = 0;
            while ((st = br.readLine()) != null) {
                count++;
                st = st.trim();
                String[] edge = st.split(" ");
                G.get(Integer.parseInt(edge[0])).add(new Edge(vertices[Integer.parseInt(edge[0])], vertices[Integer.parseInt(edge[1])], Integer.parseInt(edge[2])));
                unweightedG.get(Integer.parseInt(edge[0])).add(Integer.parseInt(edge[1]));
            }
            br.close();
            if (count < size) throw new RuntimeErrorException(null);
        } catch (IOException e) {
            throw new RuntimeErrorException(null);
        }
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return size;
    }

    @Override
    public ArrayList<Integer> getVertices() {
        // TODO Auto-generated method stub
        return (ArrayList<Integer>) verticesVal;
    }

    @Override
    public ArrayList<Integer> getNeighbors(int v) {
        // TODO Auto-generated method stub
        return (ArrayList<Integer>) unweightedG.get(v);
    }

    @Override
    public void runDijkstra(int src, int[] distances) {
        // TODO Auto-generated method stub
        PriorityQueue<Vertex> pq = new PriorityQueue<>(Comparator.comparingInt(Vertex::getDist));
        processOrder = new ArrayList<>(G.size());
        vertices[src].dist = 0;
        pq.addAll(Arrays.asList(vertices));
        while (!pq.isEmpty()) {
            Vertex min = pq.poll();
            for (Edge e : G.get(min.value)) {
                if (min.dist + e.weight < e.end.dist) {
                    pq.remove(e.end);
                    e.end.dist = min.dist + e.weight;
                    e.end.prev = e.start.value;
                    pq.add(e.end);
                }
            }
            processOrder.add(min.value);
        }
        for (int i = 0; i < G.size(); i++) {
            distances[i] = vertices[i].dist;
        }
    }

    @Override
    public ArrayList<Integer> getDijkstraProcessedOrder() {
        // TODO Auto-generated method stub
        return (ArrayList<Integer>) processOrder;
    }

    @Override
    public boolean runBellmanFord(int src, int[] distances) {
        // TODO Auto-generated method stub
        Arrays.fill(distances, Integer.MAX_VALUE / 2);
        distances[src] = 0;
        vertices[src].dist = 0;
        boolean changed = true;
        int counter = 0;
        while (changed) {
            changed = false;
            for (List<Edge> edges : G) {
                for (Edge e : edges) {
                    if (e.start.dist + e.weight < e.end.dist) {
                        e.end.dist = e.start.dist + e.weight;
                        distances[e.end.value] = e.end.dist;
                        changed = true;
                    }
                }
            }
            if (counter++ == order) return false;
        }
        return true;
    }

    static class Vertex {
        int value;
        private int dist;
        int prev;

        int getDist() {
            return dist;
        }

        Vertex(int v, int d, int p) {
            value = v;
            dist = d;
            prev = p;
        }
    }

    static class Edge {
        int weight;
        Vertex start, end;

        Edge(Vertex s, Vertex e, int w) {
            start = s;
            end = e;
            weight = w;
        }
    }

}
